import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import './custom.css';
import Home from './components/Home';
import Battle from './components/Battle';
import Popular from './components/Popular';
import NoMatch from './components/NoMatch';
import Navbar from './components/Navbar';


class App extends Component {
  render() {
    return (
        <Router className="container-fluid">
          <div className="App"> 
            <Navbar />
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/battle" component={Battle}/>
              <Route path="/popular" component={Popular}/>
              <Route component={NoMatch}/>
            </Switch>
          </div>
        </Router>     
    );
  }
}

export default App;
