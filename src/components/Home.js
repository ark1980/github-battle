import React from 'react';
import { Link } from 'react-router-dom';
import '../custom.css';

const Home = () => {
  return(
    <div className="home">
      <h1>Github Battle: Battle your friends... and stuff.</h1>
      <Link to="/battle" className="btn btn-primary btn-lg battleButton" >
        Battle
      </Link>
    </div>
  )
}

export default Home;