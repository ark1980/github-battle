import React from 'react';
import { NavLink } from 'react-router-dom';
import '../custom.css';

const Navbar = () => {
  return(
    <div className="navbar navbar-dark bg-dark">
      <ul className="nav">
        <li className="nav-item">
          <NavLink exact to="/" className="nav-link" activeClassName="activated">Home</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/battle" className="nav-link" activeClassName="activated">Battle</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/popular" className="nav-link" activeClassName="activated">Popular</NavLink>
        </li>
      </ul>
    </div>
  )
}

export default Navbar;